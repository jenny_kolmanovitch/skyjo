package BrokenLine;

import java.util.List;

import entities.Users;
public class AddContactsToGroupRequest {

	private String groupId;
	private List<Users> users;
	
	public AddContactsToGroupRequest(){};
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}
}
