package BrokenLine;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import entities.Groups;

public interface GroupRepository extends MongoRepository<Groups, String> {

    public Groups findById(String id);

}