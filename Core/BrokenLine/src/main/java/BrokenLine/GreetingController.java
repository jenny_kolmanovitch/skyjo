package BrokenLine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.collections.list.UnmodifiableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import entities.Greeting;
import entities.Groups;
import entities.Users;

@RestController
public class GreetingController {
	@Autowired
	private GroupRepository repository;
	@Autowired
	private UserRepository userRep;
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

	@RequestMapping(value = "/")
    public String index(){
        System.out.println("Home Page");
        return "index";
    }
    
    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    @RequestMapping(method = RequestMethod.POST, value="/createGroup")
    public Groups createGroup(@RequestBody Groups group ) {
		// save a couple of customers
		repository.save(group);
		List<Groups> all = repository.findAll();
		int size = all.size();
        return all.get(size-1);
    }
    @RequestMapping(method = RequestMethod.GET, value="/createGroupGet")
    public Groups createGroupGet(@RequestParam(value="groupName") String groupName) {
		// save a couple of customers
		repository.save(new Groups(groupName));
		List<Groups> all = repository.findAll();
		int size = all.size();
        return all.get(size-1);
    }
    
    @RequestMapping(value="/createUser", method=RequestMethod.POST)
    public void createUser(@RequestBody Users user) {
		userRep.save(user);
    }
    
    @RequestMapping(value="/addContactsToGroup", method=RequestMethod.POST)
    public Groups addContactsToGroup(@RequestBody AddContactsToGroupRequest requestParams)
    {
    	Groups group = repository.findOne(requestParams.getGroupId());
    	List<Users> users = requestParams.getUsers();
    	group.setUserIds(users);
    	repository.save(group);
    	for (Users user : users)
    	{
    		Groups g = new Groups();
    		g.setId(requestParams.getGroupId());
    		List<Groups> list = user.getGroupIds();
    		if (list==null)
    			list = new ArrayList<Groups>();
			list.add(g);
    		user.setGroupIds(list);
    		userRep.save(user);
    	}
    	Groups result = repository.findOne(requestParams.getGroupId());
    	return result;
    }
    
    /*@RequestMapping(value="/getUserGroups", method=RequestMethod.GET)
    public List<Groups> getUserGroups(@RequestParam String userId)
    {
    	Users user = userRep.findOne(userId);
    	return user.getGroupIds();
    }*/

}