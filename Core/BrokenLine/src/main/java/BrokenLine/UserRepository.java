package BrokenLine;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import entities.Users;

public interface UserRepository extends MongoRepository<Users, String> {

    public List<Users> findById(String id);

}