package entities;



import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Users {
    @Id
    private String id;
	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id=id;
	}
    private String firstName; 
    public String getFirstName(){
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName= firstName;
	}
    private String lastName; 
	public String getLastName(){
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName= lastName;
	}
    private String email;
    public String getEmail(){
		return email;
	}
	public void setEmail(String email)
	{
		this.email= email;
	}
    private String phone;
    public String getPhone(){
		return phone;
	}
	public void setPhone(String phone){
		this.phone = phone;
	}
    @DBRef(lazy = true)
   
    private List<Groups> groupIds;
	
	public Users(String firstName, String lastName, String email, String phone,List<Groups> groupId)
	//, List<String> groups)
	{
		this.firstName=firstName;
		this.lastName= lastName;
		this.email=email;
		this.phone = phone;
		this.groupIds=groupId;
	}
	public Users(){
		//this.groupIds = new ArrayList<Groups>();
	}
/*
	public void addGroup (String groupId){
		Groups group = new Groups();
		group.setId(groupId);
		if (this.groupIds!=null && !this.groupIds.contains(group))
			this.groupIds.add(group);
	}
	public List<Groups> getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(List<Groups> groupIds) {
		this.groupIds = groupIds;
	}

*/
	public List<Groups> getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(List<Groups> groupIds) {
		this.groupIds = groupIds;
	}
	
	
}
