package entities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class Groups {
    @Id
    private String id;
    @DBRef(lazy = true)
    private List<Users> userIds;   
    @DBRef
    private Users adminUserId;    
    private String name; 
    
    public Groups(String name)
	{
		this();
		this.name=name;
	}
	public Groups(){
		//this.userIds = new ArrayList<Users>();
	}
	
    public String getId(){
    	return id;
    }
    public void setId(String id){
    	this.id=id;
    }
    
	
	public String getName(){
		return name;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public List<Users> getUserIds() {
		return userIds;
	}
	public void setUserIds(List<Users> userIds) {
		this.userIds = userIds;
	}

	public Users getAdminUserId() {
		return adminUserId;
	}
	public void setAdminUserId(Users groupAdminId) {
		this.adminUserId = groupAdminId;
	}


	
	
}
